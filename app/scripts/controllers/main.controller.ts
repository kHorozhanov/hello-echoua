/**
 * MainCtrl
 */

class MainCtrl {
    static $inject = ["$timeout", "$filter"];
    text: string;
    constructor(private $timeout, private $filter) {
        this.text = 'Hello World!';
        $timeout(() => {
            this.text = $filter('uppercase')(this.text);
        }, 5000);
    }
}

app.controller('MainCtrl', MainCtrl);