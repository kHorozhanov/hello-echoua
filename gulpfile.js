var gulp = require('gulp');
var ts = require('gulp-typescript');
var browserSync = require('browser-sync').create();
var mainBowerFiles = require('main-bower-files');
var concat = require('gulp-concat');

var config = {
    dist: 'dist/'
};

gulp.task('scripts', function () {
    return gulp.src('app/scripts/**/*.ts')
        .pipe(ts({
            noImplicitAny: true,
            out: 'main.js'
        }))
        .pipe(gulp.dest(config.dist + 'scripts/'));
});

gulp.task('copy', function () {
    return gulp.src('app/index.html')
        .pipe(gulp.dest(config.dist));
});

gulp.task('bower', function () {
    return gulp.src(mainBowerFiles({
        filter: '**/*.js',
        paths: {
            bowerDirectory: './bower_components',
            bowerJson: './bower.json'
        }
    }))
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(config.dist + 'scripts/'));
});

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: config.dist
        }
    });
    gulp.watch("app/scripts/**/*.ts", ['scripts']);
    gulp.watch("app/index.html", ['copy']);
    gulp.watch("bower.json", ['bower']);

    gulp.watch(config.dist + '**').on("change", browserSync.reload);
});


gulp.task('default', ['scripts', 'copy', 'bower', 'browser-sync']);